class AddArchivedFlagToSeriens < ActiveRecord::Migration
  def change
    add_column :seriens, :archiveFlag, :boolean
  end
end
