class CreateSeriens < ActiveRecord::Migration
  def change
    create_table :seriens do |t|
      t.string :title
      t.integer :episode
      t.integer :newestEpisode
      t.string :release
      t.string :url

      t.timestamps null: false
    end
  end
end
