class AddNewestEpisodeToAnime < ActiveRecord::Migration
  def change
    add_column :animes, :newestEpisode, :integer
  end
end
