class CreateMangas < ActiveRecord::Migration
  def change
    create_table :mangas do |t|
      t.string :title
      t.integer :chapter
      t.integer :newestChapter
      t.string :release
      t.string :url

      t.timestamps null: false
    end
  end
end
