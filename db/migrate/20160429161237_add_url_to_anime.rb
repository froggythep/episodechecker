class AddUrlToAnime < ActiveRecord::Migration
  def change
    add_column :animes, :url, :string
  end
end
