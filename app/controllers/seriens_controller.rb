class SeriensController < ApplicationController
  def index
      @seriens = Serien.all
    end
  
  def new
  end
  
  def edit
    @serien = Serien.find(params[:id])
  end
  
  def update
    @serien = Serien.find(params[:id])
   
    if @serien.update(serien_params)
      redirect_to root_path, notice: "Series updated"
    else
      flash[:error] = "Somethig is wrong"
    end
  end
  
  def create
    @serien = Serien.new(serien_params)
     
    @serien.save
    redirect_to root_path, notice: "New Series created"
  end
  
  def destroy
    @serien = Serien.find(params[:id])
    @serien.destroy
    
    redirect_to root_path, notice: "Series deleted"
  end
  
  
  private
    def serien_params
      params.require(:serien).permit(:title, :episode, :release, :url, :archiveFlag)
    end
end
