class AnimesController < ApplicationController
  def new
  end
  
  def edit
    @anime = Anime.find(params[:id])
  end
  
  def update
    @anime = Anime.find(params[:id])
   
    if @anime.update(anime_params)
      redirect_to root_path, notice: "Anime updated"
    else
      flash[:error] = "Somethig is wrong"
    end
  end
  
  def create
    @anime = Anime.new(anime_params)
     
    @anime.save
    redirect_to root_path, notice: "New Anime created"
  end
  
  def destroy
    @anime = Anime.find(params[:id])
    @anime.destroy
    
    redirect_to root_path, notice: "Anime deleted"
  end
  
  
  private
    def anime_params
      params.require(:anime).permit(:title, :episode, :release, :url)
    end
end
