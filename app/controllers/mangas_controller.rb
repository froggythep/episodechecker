class MangasController < ApplicationController
  def new
    end
    
    def edit
      @manga = Manga.find(params[:id])
    end
    
    def update
      @manga = Manga.find(params[:id])
     
      if @manga.update(manga_params)
        redirect_to root_path, notice: "Manga updated"
      else
        flash[:error] = "Somethig is wrong"
      end
    end
    
    def create
      @manga = Manga.new(manga_params)
       
      @manga.save
      redirect_to root_path, notice: "New Manga created"
    end
    
    def destroy
      @manga = Manga.find(params[:id])
      @manga.destroy
      
      redirect_to root_path, notice: "Manga deleted"
    end
    
    
    private
      def manga_params
        params.require(:manga).permit(:title, :chapter, :release, :url)
      end
end
